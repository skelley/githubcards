import React, { Component } from 'react'
import './App.css'
import { Card, Image } from 'semantic-ui-react'


 export default class Toggle extends Component {
    constructor(props) {
        super(props)
        this.state = {
          user: {},
          active: false
        }
        this.toggleUser = () => {

          console.log(this)
          fetch('https://api.github.com/users/Shaquon')
            .then((res) => res.json())
            .then((data) => {
              console.log(data)

              const currentState = this.state.active
              this.setState({
                user: {
                  name: data.login,
                  photo: data.avatar_url,
                  bio: data.bio,
                  website: data.blog
                },
                active: !currentState
              })
            })
        }
      }
    
  render() {
    return (
        <React.Fragment>

            <button onClick={this.toggleUser} >Show/Hide </button>
          {/*the code below id a conditional and it reads: if this.state.active is true then output stuff below, if false then 
            render nothing at all*/
            this.state.active &&
        <Card>
          <Image src={this.state.user.photo} />
          <Card.Content>
            <Card.Header>{this.state.user.name}</Card.Header>
            <Card.Meta>{this.state.user.bio}</Card.Meta>
            <Card.Description>{this.state.user.website}</Card.Description>
          </Card.Content>
        </Card>}

        </React.Fragment>
    )
  }
}

